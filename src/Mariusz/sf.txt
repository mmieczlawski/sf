
_________________________________________
utwożenie bundle
php bin/console generate:bundle	--namespace=My/FrontendBundle --dir=src --no-interaction
php bin/console generate:bundle	--namespace=Buszke/LibrarydBundle --dir=src --no-interaction

czyszcenie cache
php bin/console cache:clear --env=dev


wygenerowanie entity
php bin/console generate:doctrine:entity

wygenerowanie get i set entity w przestrzeni Mariusz
php bin/console generate:doctrine:entities Mariusz
php bin/console generate:doctrine:entities Buszke

uaktualnienia bazy danych: 
php bin/console doctrine:schema:update --force


wygenerowanie CRUD, należy podać entity: MariuszMBoundle:[entity]
php bin/console generate:doctrine:crud
php bin/console generate:doctrine:crud --entity=BuszkeLibraryBundle:Borrowed --with-write


wygenerowanie plików js i css na środowisku produkcyjnym
php bin/console assetic:dump --env=prod --no-debug

wygenerowanie entity na podstawie bazy danych
http://symfony-docs.pl/cookbook/doctrine/reverse_engineering.html




http://www.merixstudio.pl/blog/fosuserbundle-biblioteka-Symfony2-bez-ktorej-sie-nie-obedziesz/
http://www.mkotlarz.pl/qrapp-instalacja-konfiguracja-fosuserbundle/

php bin/console fos:user:create** tworzenie nowego użytkownika,
php bin/console fos:user:change-password** zmiana hasła,
php bin/console fos:user:promote** nadawanie ról użytkownikowi,
php bin/console fos:user:demote** usuwanie ról użytkownikowi,
php bin/console fos:user:activate** aktywacja konta,
php bin/console fos:user:deactivate** deaktywacja konta.





