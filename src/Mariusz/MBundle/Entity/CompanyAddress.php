<?php

namespace Mariusz\MBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mariusz\MBundle\Model\EntityInterface;
use Mariusz\MBundle\Entity\Company;

/**
 * Adress
 *
 * @ORM\Table(name="company_address")
 * @ORM\Entity(repositoryClass="Mariusz\MBundle\Repository\CompanyAddressRepository")
 */
class CompanyAddress implements EntityInterface
{
    use \Mariusz\MBundle\Entity\Traits\EntityTrait;    
    use \Mariusz\MBundle\Entity\Traits\AdressTrait;    
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
 


    /**
     * @ORM\OneToOne(
     *      targetEntity="Company"
     * )
     * @ORM\JoinColumn(
     *      name="company_id", 
     *      referencedColumnName="id"
     * )
     * 
     */
    private $companes;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companes
     *
     * @param \Mariusz\MBundle\Entity\Company $companes
     *
     * @return CompanyAddress
     */
    public function setCompanes(\Mariusz\MBundle\Entity\Company $companes = null)
    {
        $this->companes = $companes;

        return $this;
    }

    /**
     * Get companes
     *
     * @return \Mariusz\MBundle\Entity\Company
     */
    public function getCompanes()
    {
        return $this->companes;
    }
}
