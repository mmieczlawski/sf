<?php

namespace Mariusz\MBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mariusz\MBundle\Model\EntityInterface;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="Mariusz\MBundle\Repository\ProjectRepository")
 */
class Project implements EntityInterface
{
    use \Mariusz\MBundle\Entity\Traits\EntityTrait;  
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="date", nullable=true)
     */
    private $dateEnd;


    /**
     * @var \integer
     *
     * @ORM\Column(name="status_id", type="integer", nullable=true, options={"default" = null})
     */
    private $statusId;
    
    /**
    * @ORM\ManyToOne(targetEntity="Dictonary", inversedBy="projects")
    * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
    */
    protected $status;       

    /**
     * @var \string
     *
     * @ORM\Column(name="descryption", type="text", nullable=true)
     */
    private $descryption;

    
    /**
    * @ORM\OneToMany(targetEntity="Task", mappedBy="tasks", cascade={"persist"})
    */
    protected $tasks;    
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Project
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Project
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tasks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add task
     *
     * @param \Mariusz\MBundle\Entity\Task $task
     *
     * @return Project
     */
    public function addTask(\Mariusz\MBundle\Entity\Task $task)
    {
        $task->setProject($this);
        $this->tasks[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param \Mariusz\MBundle\Entity\Task $task
     */
    public function removeTask(\Mariusz\MBundle\Entity\Task $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return Project
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set descryption
     *
     * @param string $descryption
     *
     * @return Project
     */
    public function setDescryption($descryption)
    {
        $this->descryption = $descryption;

        return $this;
    }

    /**
     * Get descryption
     *
     * @return string
     */
    public function getDescryption()
    {
        return $this->descryption;
    }

    /**
     * Set status
     *
     * @param \Mariusz\MBundle\Entity\Dictonary $status
     *
     * @return Project
     */
    public function setStatus(\Mariusz\MBundle\Entity\Dictonary $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Mariusz\MBundle\Entity\Dictonary
     */
    public function getStatus()
    {
        return $this->status;
    }
}
