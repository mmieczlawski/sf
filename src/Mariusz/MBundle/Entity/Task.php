<?php

namespace Mariusz\MBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mariusz\MBundle\Model\EntityInterface;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="Mariusz\MBundle\Repository\TaskRepository")
 */
class Task implements EntityInterface
{    
   use \Mariusz\MBundle\Entity\Traits\EntityTrait; 
   
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="project_id", type="integer", nullable=true)
     */
    private $projectId;

    /**
    * @ORM\ManyToOne(targetEntity="Project", inversedBy="panstwa")
    * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
    */
    protected $project;   
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;    


    /**
     * @var \string
     *
     * @ORM\Column(name="descryption", type="text", nullable=true)
     */
    private $descryption;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="date", nullable=true)
     */
    private $dateEnd;    
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectId
     *
     * @param integer $projectId
     *
     * @return Task
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId
     *
     * @return int
     */
    public function getProjectId()
    {
        return $this->projectId;
    }
    

    /**
     * Set project
     *
     * @param \Mariusz\MBundle\Entity\Project $project
     *
     * @return Task
     */
    public function setProject(\Mariusz\MBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Mariusz\MBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Task
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set descryption
     *
     * @param string $descryption
     *
     * @return Task
     */
    public function setDescryption($descryption)
    {
        $this->descryption = $descryption;

        return $this;
    }
    
    /**
     * Get descryption
     *
     * @return string
     */
    public function getDescryption()
    {
        return $this->descryption;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return Task
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Task
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }
}
