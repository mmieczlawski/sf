<?php

namespace Mariusz\MBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mariusz\MBundle\Model\EntityInterface;

/**
 * Dictonary
 *
 * @ORM\Table(name="dictonary")
 * @ORM\Entity(repositoryClass="Mariusz\MBundle\Repository\DictonaryRepository")
 */
class Dictonary implements EntityInterface
{
   
    const TYPE_PROJECT_STATUS = 'project_status';
    const TYPE_TASK_STATUS = 'task_status';
    const TYPE_COMPANY_STATUS = 'company_status';

    private static $dictonaryType = [
        'Status projektu' => Dictonary::TYPE_PROJECT_STATUS,
        'Status zadania' =>Dictonary::TYPE_TASK_STATUS,
        'Status firmy' => Dictonary::TYPE_COMPANY_STATUS,        
    ];


    
    use \Mariusz\MBundle\Entity\Traits\EntityTrait;   
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="string", columnDefinition="ENUM('project_status', 'task_status', 'company_status')", nullable=true)
     */
    private $type;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;


    /**
    * @ORM\OneToMany(targetEntity="Project", mappedBy="status", cascade={"persist"})
    */
    protected $projects;        

    /**
    * @ORM\OneToMany(targetEntity="Company", mappedBy="company_status", cascade={"persist"})
    */
    protected $companyStatus;        
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dictonaryTypeId
     *
     * @param integer $dictonaryTypeId
     *
     * @return Dictonary
     */
    public function setDictonaryTypeId($dictonaryTypeId)
    {
        $this->dictonaryTypeId = $dictonaryTypeId;

        return $this;
    }

    /**
     * Get dictonaryTypeId
     *
     * @return int
     */
    public function getDictonaryTypeId()
    {
        return $this->dictonaryTypeId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dictonary
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }    
    
    /**
     * Add project
     *
     * @param \Mariusz\MBundle\Entity\Dictonary $project
     *
     * @return Dictonary
     */
    public function addProject(\Mariusz\MBundle\Entity\Dictonary $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \Mariusz\MBundle\Entity\Dictonary $project
     */
    public function removeProject(\Mariusz\MBundle\Entity\Dictonary $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Dictonary
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add companyStatus
     *
     * @param \Mariusz\MBundle\Entity\Company $companyStatus
     *
     * @return Dictonary
     */
    public function addCompanyStatus(\Mariusz\MBundle\Entity\Company $companyStatus)
    {
        $this->companyStatus[] = $companyStatus;

        return $this;
    }

    /**
     * Remove companyStatus
     *
     * @param \Mariusz\MBundle\Entity\Company $companyStatus
     */
    public function removeCompanyStatus(\Mariusz\MBundle\Entity\Company $companyStatus)
    {
        $this->companyStatus->removeElement($companyStatus);
    }

    /**
     * Get companyStatus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyStatus()
    {
        return $this->companyStatus;
    }
    
    /**
     * Get the types of dictionaries
     * @param type $param
     * @return type
     */
    public static function getTypesDictonares() {
        return self::$dictonaryType;
    }
    
}
