<?php

namespace Mariusz\MBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mariusz\MBundle\Model\EntityInterface;

/**
 * Adress
 *
 * @ORM\Table(name="person_address")
 * @ORM\Entity(repositoryClass="Mariusz\MBundle\Repository\PersonAddressRepository")
 */
class PersonAddress  implements EntityInterface
{
    use \Mariusz\MBundle\Entity\Traits\EntityTrait;    
    use \Mariusz\MBundle\Entity\Traits\AdressTrait;    
    
        const TYPE_REGISTRATION = 'registration';
        const TYPE_CORRESPONDENCE = 'correspondence';
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var integer
     *
     * @ORM\Column(name="person_id", type="integer")
     */    
    private $personId;    
    
    /**
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="addreses")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */    
    private $person;    
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personId
     *
     * @param integer $personId
     *
     * @return PersonAdress
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;

        return $this;
    }

    /**
     * Get personId
     *
     * @return integer
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set person
     *
     * @param \Mariusz\MBundle\Entity\Person $person
     *
     * @return PersonAddress
     */
    public function setPerson(\Mariusz\MBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Mariusz\MBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
}
