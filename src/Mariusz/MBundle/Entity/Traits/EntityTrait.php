<?php

namespace Mariusz\MBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait EntityTrait
{

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $adder;

    /**
     * @var date
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $added;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $modifier;

    /**
     * @var date
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modified;

    /**
     * Set adder
     *
     * @param integer $adder
     *
     * @return Company
     */
    public function setAdder($adder)
    {
        $this->adder = $adder;

        return $this;
    }

    /**
     * Get adder
     *
     * @return integer
     */
    public function getAdder()
    {
        return $this->adder;
    }

    /**
     * Set added
     *
     * @param \DateTime $added
     *
     * @return Company
     */
    public function setAdded($added)
    {
        $this->added = $added;

        return $this;
    }

    /**
     * Get added
     *
     * @return \DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * Set modifier
     *
     * @param integer $modifier
     *
     * @return Company
     */
    public function setModifier($modifier)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return integer
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Company
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    public function setingDate()
    {
        if (!$this->id) {
            $this->setAdded(new \DateTime('now'));
        } else {
            $this->setModified(new \DateTime('now'));
        }
    }

    public function __toString()
    {
        return isset($this->name) ? $this->name . " ({$this->id})" : __CLASS__ . ' ' . " ({$this->id})";
    }

    /**
     * Sets properties using array
     *
     * @param array $data
     */
    public function fromArray(array $data)
    {
        foreach ($data as $key => $value) {
            $methodName = 'set' . ucfirst($key);
            $this->$methodName($value);
        }
    }

}
