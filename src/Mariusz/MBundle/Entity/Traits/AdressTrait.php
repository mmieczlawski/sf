<?php
namespace Mariusz\MBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait AdressTrait
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('registration', 'correspondence')", nullable=true)
     */    
    private $type;    

    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $town;

    /**
     * @var string
     *
     * @ORM\Column(name="post_code", type="string", length=11, nullable=true)
     */
    private $postCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="house_no", type="string", length=5, nullable=true)
     */
    private $houseNo;

    /**
     * @var string
     *
     * @ORM\Column(name="apartments_no", type="string", length=5, nullable=true)
     */
    private $apartmentsNo;

    
    /**
     * @var int
     *
     * @ORM\Column(name="country_id", type="integer", nullable=true)
     */        
    private $countyId;
    
    /**
     * Set type
     *
     * @param string $type
     *
     * @return Adress
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set town
     *
     * @param string $town
     *
     * @return Adress
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     *
     * @return Adress
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Adress
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set houseNo
     *
     * @param string $houseNo
     *
     * @return Adress
     */
    public function setHouseNo($houseNo)
    {
        $this->houseNo = $houseNo;

        return $this;
    }

    /**
     * Get houseNo
     *
     * @return string
     */
    public function getHouseNo()
    {
        return $this->houseNo;
    }

    /**
     * Set apartmentsNo
     *
     * @param string $apartmentsNo
     *
     * @return Adress
     */
    public function setApartmentsNo($apartmentsNo)
    {
        $this->apartmentsNo = $apartmentsNo;

        return $this;
    }

    /**
     * Get apartmentsNo
     *
     * @return string
     */
    public function getApartmentsNo()
    {
        return $this->apartmentsNo;
    }

    /**
     * Set countyId
     *
     * @param integer $countyId
     *
     * @return Adress
     */
    public function setCountyId($countyId)
    {
        $this->countyId = $countyId;

        return $this;
    }

    /**
     * Get countyId
     *
     * @return integer
     */
    public function getCountyId()
    {
        return $this->countyId;
    }

}
