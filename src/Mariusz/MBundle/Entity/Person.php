<?php

namespace Mariusz\MBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mariusz\MBundle\Model\EntityInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Mariusz\MBundle\Entity\PersonAddress;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity(repositoryClass="Mariusz\MBundle\Repository\PersonRepository")
 */
class Person implements EntityInterface
{

    use \Mariusz\MBundle\Entity\Traits\EntityTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=40, nullable=true)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="forename", type="string", length=40, nullable=true)
     */
    private $forename;

    /**
     * @var string
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('m', 'w')", nullable=true)
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="pesel", type="string", length=11, nullable=true)
     */
    private $pesel;

    /**
     * @var date
     *
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var Doctrine\Common\Collections\ArrayCollection
     *  
     * @ORM\ManyToMany(targetEntity="Company", mappedBy="persons")
     */
    protected $companes;

    /**
     * @var Doctrine\Common\Collections\ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="PersonAddress", mappedBy="person", cascade={"persist"})
     */
    protected $address;

    protected $addressRegistration = null;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Person
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set forename
     *
     * @param string $forename
     *
     * @return Person
     */
    public function setForename($forename)
    {
        $this->forename = $forename;

        return $this;
    }

    /**
     * Get forename
     *
     * @return string
     */
    public function getForename()
    {
        return $this->forename;
    }

    /**
     * Set pesel
     *
     * @param string $pesel
     *
     * @return Person
     */
    public function setPesel($pesel)
    {
        $this->pesel = $pesel;

        return $this;
    }

    /**
     * Get pesel
     *
     * @return string
     */
    public function getPesel()
    {
        return $this->pesel;
    }

    /**
     * Set sex
     *
     * @param string $sex
     *
     * @return Person
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return \string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Person
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companes = new ArrayCollection();
        $this->address = new ArrayCollection();
    }

    /**
     * Add compane
     *
     * @param \Mariusz\MBundle\Entity\Company $compane
     *
     * @return Person
     */
    public function addCompane(\Mariusz\MBundle\Entity\Company $compane)
    {
        $this->companes->add($compane);

        return $this;
    }

    /**
     * Remove compane
     *
     * @param \Mariusz\MBundle\Entity\Company $compane
     */
    public function removeCompane(\Mariusz\MBundle\Entity\Company $compane)
    {
        $this->companes->removeElement($compane);
    }

    /**
     * Get companes
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     * 
     */
    public function getCompanes()
    {
        return $this->companes;
    }

    /**
     * Add address
     *
     * @param \Mariusz\MBundle\Entity\PersonAddress $address
     *
     * @return Person
     */
    public function addAddress(\Mariusz\MBundle\Entity\PersonAddress $address)
    {
        $this->address->add($address);

        return $this;
    }

    /**
     * Remove address
     *
     * @param \Mariusz\MBundle\Entity\PersonAddress $address
     */
    public function removeAddress(\Mariusz\MBundle\Entity\PersonAddress $address)
    {
        $this->address->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getAddress()
    {
        // zamienić na getAddresses
        return $this->address;
    }

    /**
     * Get address registration
     * @return PersonAddress
     */
    public function getAddressRegistration()
    {
        if (!$this->addressRegistration) {
            foreach ($this->getAddress() as $address) {
                if ($address->getType() == PersonAddress::TYPE_REGISTRATION) {
                    $this->addressRegistration = $address;
                }
            }
            if (!$this->addressRegistration) {
                $this->addressRegistration = new PersonAddress();
                $this->addressRegistration->setType(PersonAddress::TYPE_CORRESPONDENCE);
                $this->addressRegistration->setPersonId($this->getId());
            }
        }
        return $this->addressRegistration;
    }

    public function setAddressRegistration(PersonAddress $personAddress = null)
    {

        if (!($personAddress instanceof PersonAddress)) {
           // return;
        }

        //$this->addressRegistration = $personAddress;
        $oldAddress = $this->getAddressRegistration();

        if ($oldAddress) {
            $index = $this->getAddress()->indexOf($oldAddress);
            $this->getAddress()->set($index, $personAddress);
        } else {
            $personAddress->setType(Address::CORRESPONDENCE);
            $this->addAddress($personAddress);
        }
    }

    public function bind()
    {
        if ($this->addressRegistration) {
            $this->addressRegistration->setPerson($this);
            $this->addAddress($this->addressRegistration);
        }
    }
    
}
