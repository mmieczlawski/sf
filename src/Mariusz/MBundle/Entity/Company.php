<?php

namespace Mariusz\MBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mariusz\MBundle\Model\EntityInterface;
use Mariusz\MBundle\Entity\CompanyAddress;



/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="Mariusz\MBundle\Repository\CompanyRepository")
 */
class Company implements EntityInterface
{

    use \Mariusz\MBundle\Entity\Traits\EntityTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=12)
     */
    private $nip;

    /**
     * @var \integer
     *
     * @ORM\Column(name="status_id", type="integer", nullable=true, options={"default" = null})
     */
    private $statusId;

    /**
     * @ORM\ManyToOne(targetEntity="Dictonary", inversedBy="companyStatus")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     */
    protected $status;

    /**
     * @ORM\ManyToMany(targetEntity="Person", inversedBy="companes")
     * @ORM\JoinTable(name="company_person")
     */
    private $persons;

    /**
     * @ORM\OneToOne(
     *      targetEntity="CompanyAddress", 
     *      mappedBy="companes", 
     *      cascade={"persist"}
     * )
     * */
    private $adresses;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nip
     *
     * @param string $nip
     *
     * @return Company
     */
    public function setNip($nip)
    {
        $this->nip = $nip;

        return $this;
    }

    /**
     * Get nip
     *
     * @return string
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->persons = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add person
     *
     * @param \Mariusz\MBundle\Entity\Person $person
     *
     * @return Company
     */
    public function addPerson(\Mariusz\MBundle\Entity\Person $person)
    {
        $this->persons[] = $person;

        return $this;
    }

    /**
     * Remove person
     *
     * @param \Mariusz\MBundle\Entity\Person $person
     */
    public function removePerson(\Mariusz\MBundle\Entity\Person $person)
    {
        $this->persons->removeElement($person);
    }

    /**
     * Get persons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * Set statusId
     *
     * @param integer $statusId
     *
     * @return Company
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId
     *
     * @return integer
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set status
     *
     * @param \Mariusz\MBundle\Entity\Dictonary $status
     *
     * @return Company
     */
    public function setStatus(\Mariusz\MBundle\Entity\Dictonary $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Mariusz\MBundle\Entity\Dictonary
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set adresses
     *
     * @param \Mariusz\MBundle\Entity\CompanyAddress $adresses
     *
     * @return Company
     */
    public function setAdresses(\Mariusz\MBundle\Entity\CompanyAddress $adresses = null)
    {
        $this->adresses = $adresses;

        return $this;
    }

    /**
     * Get adresses
     *
     * @return \Mariusz\MBundle\Entity\CompanyAddress
     */
    public function getAdresses()
    {
        return $this->adresses;
    }
}
