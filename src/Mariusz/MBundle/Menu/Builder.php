<?php
namespace Mariusz\MBundle\Menu;


use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        
        $menu = $factory->createItem('root', array(
            'childrenAttributes'    => array(
                'class'             => 'navbar-nav',
            ),
        ));        
        
        $menu->addChild('Home', array('route' => 'homepage'));

        // create another menu item
        $menu->addChild('Projekty', array('route' => 'project_index'));
        $menu['Projekty']->addChild('Dodaj projekt', array('route' => 'project_new'));
        
        $menu->addChild('Zadania', array('route' => 'task_index'));
        $menu['Zadania']->addChild('Dodaj zadanie', array('route' => 'task_new'));
        
        $menu->addChild('testx', array('route' => 'task_index'));
        $menu['testx']->addChild('Dodaj zadanie', array('route' => 'task_new'));

        return $menu;
    }
}
