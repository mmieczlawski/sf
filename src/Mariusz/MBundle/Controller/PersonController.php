<?php

namespace Mariusz\MBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Mariusz\MBundle\Entity\Person;
use Mariusz\MBundle\Form\PersonType;

/**
 * Company controller.
 *
 * @Route("/person")
 */
class PersonController extends Controller
{

    use \Mariusz\MBundle\Controller\Traits\ControllerTraits;

    public function __construct()
    {
        $strName = 'Person';
        $this->setFormType(PersonType::class);
        $this->init($strName);
    }

    /**
     * Lists all Person entities.
     *
     * @Route("/", name="person_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render($this->getTwigIndex(), array(
                    'items' => $this->getRepository()->findAll(),
                    'urlNew' => $this->getUrlNew(),
                    'urlEdit' => $this->getUrlEdit(),
        ));
    }

    /**
     * Creates a new Person entity.
     *
     * @Route("/new", name="person_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $entity = new Person();
        return $this->form($entity, $request);
    }

    /**
     * Displays a form to edit an existing Person entity.
     *
     * @Route("/{id}/edit", name="person_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id = null)
    {
        return $this->editActionTraits($request, $id);
    }

    /**
     * Deletes a Person entity.
     *
     * @Route("/{id}", name="person_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Person $item)
    {
        return $this->deleteActionTraits($request, $item);
    }

}
