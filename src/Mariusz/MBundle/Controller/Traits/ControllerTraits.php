<?php

namespace Mariusz\MBundle\Controller\Traits;

use Mariusz\MBundle\Model\EntityInterface;
use Symfony\Component\HttpFoundation\Request;

trait ControllerTraits
{

    protected $strControllerName = null;
    protected $strFormType = null;
    protected $strEntityName = null;
    protected $objEntityManager = null;
    protected $strUrlIndex = null;
    protected $strUrlDelete = null;
    protected $strUrlNew = null;
    protected $strUrlEdit = null;
    protected $strTwigEdit = null;
    protected $strTwigIndex = null;

    /**
     * 
     * @return string
     */
    protected function getFormType()
    {
        return $this->strFormType;
    }

    /**
     * 
     * @param string $objFormType
     */
    protected function setFormType($objFormType)
    {
        return $this->strFormType = $objFormType;
    }

    /**
     * 
     * @return string
     */
    protected function getEntityName()
    {
        return $this->strEntityName;
    }

    /**
     * 
     * @param string $objEntity
     */
    protected function setEntityName($objEntity)
    {
        return $this->strEntityName = $objEntity;
    }

    /**
     * 
     * @return string
     */
    protected function getControllerName()
    {
        return $this->strControllerName;
    }

    /**
     * 
     * @param string $objEntity
     */
    protected function setControllerName($strControllerName)
    {
        return $this->strControllerName = $strControllerName;
    }

    /**
     * 
     * @return Doctrine\Common\Persistence\ObjectManager
     */
    protected function getManager()
    {
        if (!$this->objEntityManager) {
            $this->objEntityManager = $this->getDoctrine()->getManager();
        }
        return $this->objEntityManager;
    }

    /**
     * 
     * @return Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository($strClassName = null)
    {
        if (!$strClassName) {
            $strClassName = $this->getEntityName();
        }
        return $this->getDoctrine()->getManager()->getRepository($strClassName);
    }

    /**
     * 
     * @return string
     */
    protected function getUrlIndex()
    {
        return $this->strUrlIndex;
    }

    /**
     * 
     * @return string
     */
    protected function getUrlDelete()
    {
        return $this->strUrlDelete;
    }

    /**
     * 
     * @param string $strUrlIndex
     */
    protected function setUrlIndex($strUrlIndex)
    {
        $this->strUrlIndex = $strUrlIndex;
    }

    /**
     * 
     * @param string $strUrlDelete
     */
    protected function setUrlDelete($strUrlDelete)
    {
        $this->strUrlDelete = $strUrlDelete;
    }

    /**
     * 
     * @return string
     */
    function getUrlNew()
    {
        return $this->strUrlNew;
    }

    /**
     * 
     * @return string
     */
    function getUrlEdit()
    {
        return $this->strUrlEdit;
    }

    /**
     * 
     * @param string $strUrlNew
     */
    function setUrlNew($strUrlNew)
    {
        $this->strUrlNew = $strUrlNew;
    }

    /**
     * 
     * @param string $strUrlEdit
     */
    function setUrlEdit($strUrlEdit)
    {
        $this->strUrlEdit = $strUrlEdit;
    }

    /**
     * 
     * @return string
     */
    protected function getTwigEdit()
    {
        return $this->strTwigEdit;
    }

    /**
     * 
     * @return string
     */
    protected function getTwigIndex()
    {
        return $this->strTwigIndex;
    }

    /**
     * 
     * @param string $strTwigEdit
     */
    protected function setTwigEdit($strTwigEdit)
    {
        $this->strTwigEdit = $strTwigEdit;
    }

    /**
     * 
     * @param string $strTwigIndex
     */
    protected function setTwigIndex($strTwigIndex)
    {
        $this->strTwigIndex = $strTwigIndex;
    }

    /**
     * Creates a form to delete a entity.
     * @param EntityInterface $objEntity 
     * @return \Symfony\Component\Form\Form The form
     */
    protected function createDeleteForm(EntityInterface $objEntity)
    {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($this->getUrlDelete(), array('id' => $objEntity->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    /**
     * 
     * @param Task $objTask
     * @param Request $request
     * @param type $objManager
     * @return type
     */
    protected function form(EntityInterface $objEntity, Request $request, $objManager = null)
    {
        $form = $this->createForm($this->getFormType(), $objEntity);
        $deleteForm = $objEntity->getId() ? $this->createDeleteForm($objEntity)->createView() : null;
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            if (!$objManager) {
                $objManager = $this->getManager();
            }
            
            $objEntity = $form->getData();
            
            $objEntity->bind();
            $objManager->persist($objEntity);
            $objManager->flush();

            $this->addFlash(
                    'notice', $this->getControllerName() . ' saved');

            return $this->redirectToRoute($this->getUrlIndex());
        }

        return $this->render($this->getTwigEdit(), array(
                    'form' => $form->createView(),
                    'delete_form' => $deleteForm,
                    'id' => $objEntity->getId(),
                    'formName' => $this->getControllerName(),
                    'urlIndex' => $this->getUrlIndex(),
                    'urlNew' => $this->getUrlNew(),
                    'urlEdit' => $this->getUrlEdit(),
        ));
    }

    protected function indexActionTraits($arrParams = [])
    {
        return $this->render($this->getTwigIndex(), array_merge(
                                [
                                    'urlNew' => $this->getUrlNew(),
                                    'urlEdit' => $this->getUrlEdit(),
                                    'formName' => $this->getControllerName(),                                    
                                ], 
                                    $arrParams
                        )
        );
    }

    protected function deleteActionTraits(Request $request, EntityInterface $objEntity)
    {
        $form = $this->createDeleteForm($objEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($objEntity);
            $em->flush();
        }

        return $this->redirectToRoute($this->getUrlIndex());
    }

    protected function editActionTraits(Request $request, $id = null)
    {
        $objEntity = $this->getRepository()->find($id);
        if (!$objEntity) {
            throw $this->createNotFoundException('Record not found!');
        }
        return $this->form($objEntity, $request);
    }

    protected function init($strName)
    {
        $this->setEntityName('MariuszMBundle:' . $strName);
        $this->setControllerName($strName);

        $this->setUrlDelete(strtolower($strName) . '_delete');
        $this->setUrlIndex(strtolower($strName) . '_index');
        $this->setUrlEdit(strtolower($strName) . '_edit');
        $this->setUrlNew(strtolower($strName) . '_new');

        $this->setTwigEdit('MariuszMBundle:' . $strName . ':form.html.twig');
        $this->setTwigIndex('MariuszMBundle:' . $strName . ':index.html.twig');
    }

}
