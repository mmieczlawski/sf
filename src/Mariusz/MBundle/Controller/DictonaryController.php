<?php

namespace Mariusz\MBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Mariusz\MBundle\Entity\Dictonary;
use Mariusz\MBundle\Form\DictonaryType;

/**
 * Dictonary controller.
 *
 * @Route("/dictonary")
 */
class DictonaryController extends Controller {

    use \Mariusz\MBundle\Controller\Traits\ControllerTraits;
    
    public function __construct() {
        $strName = 'Dictonary';
        $this->setFormType(DictonaryType::class);        
        $this->init($strName);        
    }

    /**
     * Lists all Dictonary entities.
     *
     * @Route("/", name="dictonary_index")
     * @Method("GET")
     */
    public function indexAction() {
        return $this->render($this->getTwigIndex(), array(
                    'dictonaries' => $this->getRepository()->findAll(),
        ));
    }

    /**
     * Creates a new Dictonary entity.
     *
     * @Route("/new", name="dictonary_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $dictonary = new Dictonary();
        return $this->form($dictonary, $request);
    }

    /**
     * Displays a form to edit an existing Dictonary entity.
     *
     * @Route("/{id}/edit", name="dictonary_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id = null) {
        
        return $this->editActionTraits($request, $id);
    }

    
    /**
     * Deletes a Dictonary entity.
     *
     * @Route("/{id}/delete", name="dictonary_delete")
     * @Method({"DELETE"})
     */
    public function deleteAction(Request $request, Dictonary $dictonary) {        
        return $this->deleteActionTraits($request, $dictonary);
    }


}
