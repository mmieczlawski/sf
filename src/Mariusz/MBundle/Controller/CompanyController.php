<?php

namespace Mariusz\MBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Mariusz\MBundle\Entity\Company;
use Mariusz\MBundle\Form\CompanyType;

/**
 * Company controller.
 *
 * @Route("/company")
 */
class CompanyController extends Controller
{

    use \Mariusz\MBundle\Controller\Traits\ControllerTraits;

    public function __construct()
    {

        $strName = 'Company';
        $this->setFormType(CompanyType::class);
        $this->init($strName);
    }

    /**
     * Lists all Company entities.
     *
     * @Route("/", name="company_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        return $this->render($this->getTwigIndex(), array(
                    'items' => $this->getRepository()->findAll(),
                    'urlNew' => $this->getUrlNew(),
                    'urlEdit' => $this->getUrlEdit(),
        ));
    }

    /**
     * Creates a new Company entity.
     *
     * @Route("/new", name="company_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request) {
        $entity = new Company();
        return $this->form($entity, $request);
    }

    /**
     * Displays a form to edit an existing Company entity.
     *
     * @Route("/{id}/edit", name="company_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id = null)
    {
        return $this->editActionTraits($request, $id);
    }

    /**
     * Deletes a Company entity.
     *
     * @Route("/{id}", name="company_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Company $item) {        
        return $this->deleteActionTraits($request, $item);
    }

}
