<?php

namespace Mariusz\MBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

use Mariusz\MBundle\Entity\Project;
use Mariusz\MBundle\Form\ProjectType;


/**
 * Description of ProjectController
 *
 * @author m.mieczlawski
 * @Route("/projects")
 */
class ProjectController  extends Controller
{
    
    use \Mariusz\MBundle\Controller\Traits\ControllerTraits;

    public function __construct()
    {
        $strName = 'Project';
        $this->setFormType(ProjectType::class);
        $this->init($strName);
    }
    
    
    /**
     * @Route("/", name="project_index")
     */
    public function indexAction()
    {
        return $this->render($this->getTwigIndex(), array(
                    'items' => $this->getRepository()->findAll(),
                    'urlNew' => $this->getUrlNew(),
                    'urlEdit' => $this->getUrlEdit(),
        ));
    }    
    
    /**
     * @Route("/project{id}", name="project_edit")
     */
    public function editAction(Request $request, $id = null)
    {
        return $this->editActionTraits($request, $id);
    }    
    

    /**
     * @Route("/new", name="project_new")
     */
    public function newAction(Request $request)
    {
        $entity = new Project();
        return $this->form($entity, $request);
    }    

    /**
     * Deletes a Project entity.
     *
     * @Route("/{id}", name="project_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Project $item)
    {
        return $this->deleteActionTraits($request, $item);
    }    
    
}
