<?php

namespace Mariusz\MBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Mariusz\MBundle\Entity\Task;
use Mariusz\MBundle\Form\TaskType;


/**
 * Description of TaskController
 *
 * @author m.mieczlawski
 * @Route("/tasks")
 */
class TaskController extends Controller
{

    use \Mariusz\MBundle\Controller\Traits\ControllerTraits;

    public function __construct()
    {
        $strName = 'Task';
        $this->setFormType(TaskType::class);
        $this->init($strName);
    }
    
    /**
     * @Route("/", name="task_index")
     */
    public function indexAction()
    {        
        return $this->indexActionTraits(['items' => $this->getRepository()->findAll()]);
    }

    /**
     * @Route("/task{id}", name="task_edit")
     */
    public function editAction(Request $request, $id = null)
    {
        return $this->editActionTraits($request, $id);
    }

    /**
     * @Route("/new", name="task_new")
     */
    public function newAction(Request $request)
    {
        $objTask = new \Mariusz\MBundle\Entity\Task();
        $objTask->setName('Zadanie pierwsze');
        $objTask->setDateStart(new \DateTime('tomorrow'));
        $objTask->setDateEnd(new \DateTime('tomorrow'));

        return $this->form($objTask, $request);
    }


    /**
     * Deletes a Task entity.
     *
     * @Route("/{id}", name="task_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Task $item) {        
        return $this->deleteActionTraits($request, $item);
    }
    
 
}
