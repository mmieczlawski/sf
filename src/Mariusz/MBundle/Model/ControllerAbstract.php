<?php
namespace Mariusz\MBundle\Model;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Mariusz\MBundle\Entity\Dictonary;
use Mariusz\MBundle\Form\DictonaryType;
use Mariusz\MBundle\Model\EntityInterface;

use Symfony\Component\Form\AbstractType;


/**
 * Description of ControllerAbstract
 *
 * @author mariusz
 */
class ControllerAbstract extends Controller{
    
    protected $objFormType = null;
    protected $objEntity = null;
    protected $objEntity = null;
    protected $objEntityManager = null;




    /**
     * 
     * @return AbstractType
     */
    protected function getFormType () {
        return $this->objFormType;
    }
    
    /**
     * 
     * @param AbstractType $objFormType
     * @return AbstractType
     */
    protected function setFormType (AbstractType $objFormType) {
        return $this->objFormType = $objFormType;
    }    

    /**
     * 
     * @param Task $objTask
     * @param Request $request
     * @param type $manager
     * @return type
     */
    private function form(EntityInterface $objEntity, Request $request, $manager = null) {
        $form = $this->createForm($this->getFormType(), $objEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$manager) {
                $manager = $this->getDoctrine()->getManager();
            }

            $objEntity = $form->getData();
            $manager->persist($objEntity);
            $manager->flush();

            $this->addFlash(
                    'notice', $this->strControllerName .  ' zapisano');

            return $this->redirectToRoute('dictonary_index');
        }

        return $this->render('MariuszMBundle:dictonary:new.html.twig', array(
                    'form' => $form->createView(),
                    'id' => $objEntity->getId()
        ));
    }
    
}
