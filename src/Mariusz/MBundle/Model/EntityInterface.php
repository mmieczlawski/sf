<?php
namespace Mariusz\MBundle\Model;

interface EntityInterface {
    public function getId();
}
