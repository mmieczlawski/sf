<?php

namespace Mariusz\MBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Description of TaskType
 *
 * @author m.mieczlawski
 */
class ProjectType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
                ->add('name', TextType::class)
                ->add('dateStart', DateType::class, array('widget' => 'single_text'))
                ->add('dateEnd', DateType::class, array('widget' => 'single_text'))
                ->add('descryption', TextareaType::class)
                ->add('status')
                ->add('status', EntityType::class, array(
                    'class' => 'Mariusz\MBundle\Entity\Dictonary',
                    'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
                        return $er->getDictonaryByType(\Mariusz\MBundle\Entity\Dictonary::TYPE_PROJECT_STATUS);
                    },
                    'choice_label' => 'name',
                        )
                )
        ;
    }

    public function getName()
    {
        return 'project';
    }

}
