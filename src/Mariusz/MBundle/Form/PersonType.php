<?php

namespace Mariusz\MBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Mariusz\MBundle\Repository\DictonaryRepository;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Mariusz\MBundle\Form\PersonAddressType;


/**
 * Description of PersonType
 *
 * @author mariuszm
 */
class PersonType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('surname')
                ->add('forename')
                ->add('sex', ChoiceType::class, array(
                    'choices' => DictonaryRepository::getGender(),
                    'required' => false,
                    'placeholder' => 'Choose your gender',
                    'empty_data' => null
                ))
                ->add('pesel')
                ->add('birth_date', DateType::class, array(
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                ))
                ->add('addressRegistration', PersonAddressType::class, [
                    'label' => false,
                    'property_path' => 'addressRegistration',
                ])                
                
                ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mariusz\MBundle\Entity\Person'
        ));
    }

}
