<?php

namespace Mariusz\MBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Mariusz\MBundle\Form\CompanyAddressType;

class CompanyType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name')
                ->add('nip')
                ->add('status', EntityType::class, array(
                    'class' => 'Mariusz\MBundle\Entity\Dictonary',
                    'query_builder' => function (\Doctrine\ORM\EntityRepository $er) {
                        return $er->getDictonaryByType(\Mariusz\MBundle\Entity\Dictonary::TYPE_COMPANY_STATUS);
                    },
                    'choice_label' => 'name',
                    'placeholder' => '- select options -',
                        )
                )
                //->add('persons')
                ->add('adresses', CompanyAddressType::class, [
                    'label' => false,
                ])             
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mariusz\MBundle\Entity\Company'
        ));
    }

}
