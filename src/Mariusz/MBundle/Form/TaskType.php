<?php

namespace Mariusz\MBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Description of TaskType
 *
 * @author m.mieczlawski
 */
class TaskType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//                ->add('project_id', ChoiceType::class, array(
//                    'choices'   => array(
//                        'Morning'   => 4,
//                        'Afternoon' => 5,
//                        'Evening'   => 6,
//                    ),
//                    'multiple'  => false,
//                ))                
                ->add('project')
                ->add('name', TextType::class)
                ->add('descryption', TextareaType::class)
                ->add('dateStart', DateType::class, array('widget' => 'single_text'))
                ->add('dateEnd', DateType::class, array('widget' => 'single_text'))
                #->add('dateEnd2', DateType::class, array('mapped' => false))
        ;
    }

    public function getName()
    {
        return 'task';
    }

}
