<?php

namespace Mariusz\MBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


/**
 * Description of PersonAddressType
 *
 * @author m.mieczlawski
 */
class PersonAddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//                ->add('type')
                ->add('town')
                ->add('post_code')
                ->add('street')
                ->add('house_no')
                ->add('apartments_no')
//                ->add('country_id')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mariusz\MBundle\Entity\PersonAddress'
        ));
    }
    
    /**
     * 
     * @return string
     */
    public function getName()
    {
        return 'Person_address';
    }    
    
}
